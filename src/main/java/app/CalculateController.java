package app;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculateController {


    @RequestMapping("/square")
    public String getSquare(@RequestParam("num") int num){
        int square = num*num;
        return "Square of " + num +" is " +square;
    }

    @RequestMapping("/sum")
    public String getSum(@RequestParam("a") int a, @RequestParam("b") int b){
        int sum = a+b;
        return "Sum of " + a + " and " + b + " is " + sum;
    }

}
